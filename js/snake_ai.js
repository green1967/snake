var LEARN = 1;
var canvas, ctx;

var Apple = function(pos, type) {
    this.p = pos;
    this.type = type;
    this.cleanup_=false;
    this.age=0;
}

var World = function() {
    this.agents = [];
    this.W = 30;
    this.H = 50;

    this.clock = 0;

    // draw walls
    let i=0;
    let str = '';
    while(i<=this.W*this.H-1) 
    {
    if(!(i/this.W-Math.trunc(i/this.W)))
    str = "<div class=\"box\" id=\"" + i + "\" style=\"background-color:brown\"></div>";
    else
    if (!((i+1)/this.W-Math.trunc((i+1)/this.W)))
    str = "<div class=\"box\" id=\"" + i + "\" style=\"background-color:brown\"></div>";
    else
    str = "<div class=\"box\" id=\"" + i + "\" style=\"background-color:#eee\"></div>";
    if(i<=this.W || i>=(this.W*this.H-this.W-1))
    str = "<div class=\"box\" id=\"" + i + "\" style=\"background-color:brown\"></div>";
    box.insertAdjacentHTML("afterbegin", str);
    i++;
    }
    this.eaten_items = 0;
    this.items = []
    this.i_count = LEARN ? 20 : 1; 
    for(var k=0;k<this.i_count;k++) {
    if(LEARN) var p = convnetjs.randi(31, this.W*this.H-32);
    else var p = convnetjs.randi(91, this.W*this.H-92);;
  //  if(!k) p = 31;
      if(document.getElementById(p).style.backgroundColor != 'brown' && document.getElementById(p).style.backgroundColor != 'green') {var it = new Apple(p, 1);
      this.items.push(it);}
    }

}

World.prototype = {
    stuff_collide_: function(p, d, r, check_walls, check_items) {
        var res = {};
        var minres = false;
        var step = null;

        // collide with walls
        switch(d) {
        case 111:
            step = 30;
            break;
        case 100:
            step = -30;
            break;
        case 101:
            step = -1;
            break;
        case 110:
            step = 1;
            break;
        }

        var i = p + step;
        const max_range = i + r*step;
        
        while(max_range-i) {
//         console.log('d=', d, 'i=', i, 'p=', p, 'step=', step, 'max_range=', max_range,'w.el=', document.getElementById(i).style.backgroundColor);   
            if(check_items) {
                if(document.getElementById(i).style.backgroundColor =='red') {
                    res.type=1; 
                    res.dis= p<i ? (i-p)/Math.abs(step) : (p-i)/Math.abs(step);
                    minres = res;
                    return minres;    
                    }
                if(document.getElementById(i).style.backgroundColor =='green') {
                    res.type=0; 
                    res.dis= p<i ? (i-p)/Math.abs(step) : (p-i)/Math.abs(step); 
                    minres=res;
                    return minres;
                }
            }
            if(check_walls) {
                if(document.getElementById(i).style.backgroundColor =='brown') {
                    res.type=0; 
                    res.dis= p<i ? (i-p)/Math.abs(step) : (p-i)/Math.abs(step);
                    minres = res;
 //                   console.log("w.i=", i, 'w.p=', p, 'w.el=', document.getElementById(i).style.backgroundColor, 'res=', res);
                    return minres;
                    }
            }
            i= i + step; 
            continue};
       },
    tick: function() {
        this.clock++;

        // fix input to all agents based on environment
        // process eyes
 //       this.collpoints = [];
        if(this.clock<2) {
        for(var i=0,n=this.agents.length;i<n;i++) {
          var a = this.agents[i];
          for(var ei=0,ne=a.eyes.length;ei<ne;ei++) {
            var e = a.eyes[ei];
            // we have a line from p to p->eyep
//            console.log('tik.e.p=', e);
            var res = this.stuff_collide_(a.p, e.direction, e.max_range, true, true);
//            if(res && res.type == 1 ) console.log("a.p=", a.p, "e.dir=", e.direction, 'res=', res);
            if(res) {              
              // eye collided with wall
              e.sensed_proximity = res.dis;
              e.sensed_type = res.type;
            } else {
              e.sensed_proximity = e.max_range;
              e.sensed_type = -1;
            }
          }
        }
      }
        
        // let the agents behave in the world based on their input
        for(var i=0,n=this.agents.length;i<n;i++) {
          this.agents[i].forward();
        }
        
        // apply outputs of agents on evironment
        for(var i=0,n=this.agents.length;i<n;i++) {
          var a = this.agents[i];
          a.op = a.p; // back up old position
          a.od = a.direction; // and direction        
                
          // agent is trying to move from p to op. Check walls
          if(document.getElementById(a.p+a.nextHop).style.backgroundColor !='brown') {
             // steer the agent according to outputs
                a.p = a.p + a.nextHop;
          //      console.log('a.p=', a.p, 'a.nextHope=', a.nextHop);
                if(document.getElementById(a.p).style.backgroundColor=='green') {a.digestion_signal += -6.0; // ewww poison
                    if(!LEARN) {console.log("SHAKE WAS EATEN!!!");
                    window.clearInterval(current_interval_id);}
            }
           //     console.log('a.disegtion_signal=', a.digestion_signal);
                a.snakeArray.splice(0,0,a.p);
          //      a.snakeArray.splice(a.snakeArray.length-1,1);
                document.getElementById(a.p).style.backgroundColor='green';
                document.getElementById(a.snakeArray.splice(a.snakeArray.length-1,1)).style.backgroundColor='#eee';
                if(a.snakeArray[0]-a.snakeArray[1] == 30) {
                    a.direction = 111;
                    a.actions[0] = 30;
                    a.actions[1] = -1;
                    a.actions[2] = 1;
                    a.actions[3] = -30;
                    }
                if(a.snakeArray[0]-a.snakeArray[1] == 1) {
                    a.direction = 110;
                    a.actions[0] = 1;
                    a.actions[1] = 30;
                    a.actions[2] = -30;
                    a.actions[3] = -1;
                    }
                if(a.snakeArray[0]-a.snakeArray[1] == -1) {
                    a.direction = 101;
                    a.actions[0] = -1;
                    a.actions[1] = -30;
                    a.actions[2] = 30;
                    a.actions[3] = 1;
                    }
                if(a.snakeArray[0]-a.snakeArray[1] == -30) {
                    a.direction = 100;
                    a.actions[0] = -30;
                    a.actions[1] = 1;
                    a.actions[2] = -1;
                    a.actions[3] = 30;
                    }
                a.updateEyesDirection();         
          }
          else {
            a.digestion_signal += -8.0;    
          console.log("ERROR: Try break the wall!!!!");
          if(!LEARN) window.clearInterval(current_interval_id);
          }
        }

        if(this.clock>1) {
          for(var i=0,n=this.agents.length;i<n;i++) {
            var a = this.agents[i];
            for(var ei=0,ne=a.eyes.length;ei<ne;ei++) {
              var e = a.eyes[ei];
              // we have a line from p to p->eyep
  //            console.log('tik.e.p=', e);
              var res = this.stuff_collide_(a.p, e.direction, e.max_range, true, true);
  //            if(res && res.type == 1 ) console.log("a.p=", a.p, "e.dir=", e.direction, 'res=', res);
              if(res) {              
                // eye collided with wall
                e.sensed_proximity = res.dis;
                e.sensed_type = res.type;
              } else {
                e.sensed_proximity = e.max_range;
                e.sensed_type = -1;
              }
            }
          }
        }
        
        // tick all items
        var update_items = false;
        
        for(var i=0,n=this.items.length;i<n;i++) {
 //           console.log(i, '   ',this.items.length);
          var it = this.items[i];
 //         console.log('it=', it);
 //         it.age += 1;
          
          // see if some agent gets lunch
          for(var j=0,m=this.agents.length;j<m;j++) {
            var a = this.agents[j];
            if(it.p == a.p) {              
                // ding! nom nom nom
                if(it.type === 1) {a.digestion_signal += 5.0; // mmm delicious apple
                this.eaten_items++;
                debug.innerHTML = this.eaten_items;
                if(a.snakeArray.length < 30 && LEARN) a.snakeArray.splice(0,0,it.p);
                if(!LEARN) a.snakeArray.splice(0,0,it.p);
                }
                if(it.type === 2) a.digestion_signal += -10.0; // ewww poison
                it.cleanup_ = true;
                update_items = true;
                break; // break out of loop, item was consumed
              
            }
          }
        }

        if(update_items) {
            var nt = [];
            for(var i=0,n=this.items.length;i<n;i++) {
              var it = this.items[i];
              if(!it.cleanup_) nt.push(it);
            }
            this.items = nt; // swap
        }
        if(LEARN) {
        if (!(this.clock % 10000)) this.i_count = 21 - Math.trunc(this.clock/10000);}
        else this.i_count = 1;
        if(this.items.length<this.i_count) {
        if(LEARN) var p = convnetjs.randi(31, this.W*this.H-32);
        else var p = convnetjs.randi(91, this.W*this.H-92);
    //        if(this.items.length == 1) p = 31;
            if(document.getElementById(p).style.backgroundColor != 'brown' && document.getElementById(p).style.backgroundColor != 'green') { var newit = new Apple(p, 1);
            this.items.push(newit); }
          }
        
        // agents are given the opportunity to learn based on feedback of their action on environment
        for(var i=0,n=this.agents.length;i<n;i++) {
          this.agents[i].backward();
     
        }
    }
}

var Eye = function() {
    this.sensed_type = -1; // what does the eye see?
    this.direction = null; 
    this.max_range = 6;
    this.sensed_proximity = 6;
  }

    // A single agent
var Agent = function() {
    
        // positional information
        this.snakeArray=[93, 63, 33];
        this.p = this.snakeArray[0];
        this.op = this.p; // old position
        this.direction = 111;
        this.od = this.direction;
        
        this.actions = [];
        this.actions.push(30);        
        this.actions.push(-1);
        this.actions.push(1);
        this.actions.push(-30);
        
        
        // properties
        this.eyes = [];
        for(var k=0;k<3;k++) {             
            this.eyes.push(new Eye()); 
        }
        this.updateEyesDirection();
        
        // braaain
        //this.brain = new deepqlearn.Brain(this.eyes.length * 3, this.actions.length);
         var spec = document.getElementById('qspec').value;
         eval(spec);
         this.brain = brain;
        
        this.reward_bonus = 0.0;
        this.digestion_signal = 0.0;
        //snakeArray.forEach(item => document.getElementById(item).style.backgroundColor='green');
        
        this.nextHop = -1;
        this.prevactionix = -1;
    }

Agent.prototype = {
        updateEyesDirection: function() {
            switch(this.direction) {
                case 111:
                    this.eyes[0].direction = 111;
                    this.eyes[1].direction = 101;
                    this.eyes[2].direction = 110;
                    break;
                case 100:
                    this.eyes[0].direction = 100;
                    this.eyes[1].direction = 110;
                    this.eyes[2].direction = 101;
                    break;
                case 110:
                    this.eyes[0].direction = 110;
                    this.eyes[1].direction = 111;
                    this.eyes[2].direction = 100;
                    break;
                case 101:
                    this.eyes[0].direction = 101;
                    this.eyes[1].direction = 100;
                    this.eyes[2].direction = 111;
                    break;
            }
        },
        forward: function() {
          // in forward pass the agent simply behaves in the environment
          // create input to brain
          var num_eyes = this.eyes.length;
          var input_array = new Array(num_eyes * 3);
          for(var i=0;i<num_eyes;i++) {
            var e = this.eyes[i];
            input_array[i*3] = 1.0;
            input_array[i*3+1] = 1.0;
            input_array[i*3+2] = 1.0;
            if(e.sensed_type !== -1) {
              // sensed_type is 0 for wall, 1 for food and 2 for poison.
              // lets do a 1-of-k encoding into the input array
 //             console.log('i=', i, 'e.sens_type=', e.sensed_type, 'e.sens_prox=', e.sensed_proximity);
              input_array[i*3 + e.sensed_type] = e.sensed_proximity/e.max_range; // normalize to [0,1]
            }
          }
          
          // get action from brain
          var actionix = this.brain.forward(input_array);
//          console.log("head_direction= ", this.direction, "input array= ", input_array, "brain.actionix=", actionix, 'actions=', this.actions);
          this.actionix = actionix; //back this up
          var action = this.actions[actionix];
          this.nextHop = action;
 //         console.log('action=', action ,'next_hop=', this.nextHop);
        },
        backward: function() {
          // in backward pass agent learns.
          // compute reward 
          var proximity_reward = 0.0;
          var num_eyes = this.eyes.length;
          for(var i=0;i<num_eyes;i++) {
            var e = this.eyes[i];
            // agents dont like to see walls, especially up close
            proximity_reward += e.sensed_type === 0 ? e.sensed_proximity/e.max_range : 1.0;
          }
          proximity_reward = proximity_reward/num_eyes;
          
          proximity_reward = Math.min(1.0, proximity_reward * 1.3);
          
          // agents like to go straight forward
          var forward_reward = 0.0;
          if(this.actionix === 0 && proximity_reward > 0.7) forward_reward = 0.2 * proximity_reward;
          
          // agents like to eat good things
          var digestion_reward = this.digestion_signal;
          this.digestion_signal = 0.0;
          
          var reward = proximity_reward + forward_reward + digestion_reward;
//          debug.innerHTML = proximity_reward;
//          if(digestion_reward > 0) console.log('reward =', reward,'proximity_reward=', proximity_reward, 'forward_rewards=', forward_reward, 'digestion_rewards=', digestion_reward);
          
          // pass to brain for learning
          this.brain.backward(reward);
        }
    }

    function draw_net() {
        if(simspeed <=1) {
          // we will always draw at these speeds
        } else {
          if(w.clock % 50 !== 0) return;  // do this sparingly
        }
        
        var canvas = document.getElementById("net_canvas");
        var ctx = canvas.getContext("2d");
        var W = canvas.width;
        var H = canvas.height;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        var L = w.agents[0].brain.value_net.layers;
        var dx = (W - 50)/L.length;
        var x = 10;
        var y = 40;
        ctx.font="12px Verdana";
        ctx.fillStyle = "rgb(0,0,0)";
        ctx.fillText("Value Function Approximating Neural Network:", 10, 14);
        for(var k=0;k<L.length;k++) {
          if(typeof(L[k].out_act)==='undefined') continue; // maybe not yet ready
          var kw = L[k].out_act.w;
          var n = kw.length;
          var dy = (H-50)/n;
          ctx.fillStyle = "rgb(0,0,0)";
          ctx.fillText(L[k].layer_type + "(" + n + ")", x, 35);
          for(var q=0;q<n;q++) {
            var v = Math.floor(kw[q]*100);
            if(v >= 0) ctx.fillStyle = "rgb(0,0," + v + ")";
            if(v < 0) ctx.fillStyle = "rgb(" + (-v) + ",0,0)";
            ctx.fillRect(x,y,10,10);
            y += 12;
            if(y>H-25) { y = 40; x += 12};
          }
          x += 50;
          y = 40;
        }
      }
      
      var reward_graph = new cnnvis.Graph();
      function draw_stats() {
        var canvas = document.getElementById("vis_canvas");
        var ctx = canvas.getContext("2d");
        var W = canvas.width;
        var H = canvas.height;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        var a = w.agents[0];
        var b = a.brain;
        var netin = b.last_input_array;
        ctx.strokeStyle = "rgb(0,0,0)";
        //ctx.font="12px Verdana";
        //ctx.fillText("Current state:",10,10);
        ctx.lineWidth = 10;
        ctx.beginPath();
        for(var k=0,n=netin.length;k<n;k++) {
          ctx.moveTo(10+k*12, 120);
          ctx.lineTo(10+k*12, 120 - netin[k] * 100);
        }
        ctx.stroke();
        
        if(w.clock % 200 === 0) {
          reward_graph.add(w.clock/200, b.average_reward_window.get_average());
          var gcanvas = document.getElementById("graph_canvas");
          reward_graph.drawSelf(gcanvas);
        }
      }

    function draw() {
        for(var i=0,n=w.agents.length;i<n;i++) {
            var a = w.agents[i];
            a.snakeArray.forEach(item => document.getElementById(item).style.backgroundColor='green');
        }
        for(var i=0,n=w.items.length;i<n;i++) {
            var it = w.items[i];
            document.getElementById(it.p).style.backgroundColor='red';
        }

        w.agents[0].brain.visSelf(document.getElementById('brain_info_div'));
    }

 // Tick the world
 function tick() {
    
 //   console.log('clock=', w.clock);   
    if(!skipdraw || w.clock % 50 === 0) {
      draw(); 
      draw_stats();
      draw_net();
    }
    w.tick();
//    if(w.clock>1) window.clearInterval(current_interval_id);
//    console.log(w.clock);
  }
  
  var simspeed = 2;
  function goveryfast() {
    window.clearInterval(current_interval_id);
    current_interval_id = setInterval(tick, 0);
    skipdraw = true;
    simspeed = 3;
  }
  function gofast() {
    window.clearInterval(current_interval_id);
    current_interval_id = setInterval(tick, 0);
    skipdraw = false;
    simspeed = 2;
  }
  function gonormal() {
    window.clearInterval(current_interval_id);
    current_interval_id = setInterval(tick, 30);
    skipdraw = false;
    simspeed = 1;
  }
  function goslow() {
    window.clearInterval(current_interval_id);
    current_interval_id = setInterval(tick, 1000);
    skipdraw = false;
    simspeed = 0;
  }
  
  function savenet() {
    var j = w.agents[0].brain.value_net.toJSON();
    var t = JSON.stringify(j);
    document.getElementById('tt').value = t;
  }
  
  function loadnet() {
    var t = document.getElementById('tt').value;
    var j = JSON.parse(t);
    w.agents[0].brain.value_net.fromJSON(j);
    stoplearn(); // also stop learning
    gonormal();
  }
  
  function startlearn() {
    w.agents[0].brain.learning = true;
  }
  function stoplearn() {
    w.agents[0].brain.learning = false;
  }
  
  function reload() {
    w.agents = [new Agent()]; // this should simply work. I think... ;\
  //  reward_graph = new cnnvis.Graph(); // reinit
  }

  function test() {
      var tt = document.getElementById('tt').value.length;
      if(tt == 3)  {savenet();console.log('SAVED');}
      w.eaten_items = 0;
      LEARN = 0; 
      w.items.map(item=>document.getElementById(item.p).style.backgroundColor="#eee");
      w.items.length = 0;
      w.agents.map(item=>item.snakeArray.map(i=>document.getElementById(i).style.backgroundColor="#eee"));
      w.agents.length = 0;
      reload();
      loadnet();
  }
  
  var w; // global world object
  var current_interval_id;
  var skipdraw = false;
  function start() {
//    canvas = document.getElementById("canvas");
//    ctx = canvas.getContext("2d");
     
    w = new World();
    w.agents = [new Agent()];
    
    gofast();
  }
